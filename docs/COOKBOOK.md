# `▶ Livre de recettes`

Ce livre de recettes fournit des aides et astuces pour vous aider à configurer
et personnaliser votre terminale et `powerline.bash`.


## `▶ Configuration`

powerline.bash prends en compte quelques variables dynamiquement, sans
recharger le shell. Pas besoin de les exporter dans l'environnement.


`POWERLINE_ONELINE`. Si `true`, affichage sur une ligne (segments puis prompt)
comme le projet powerline shell original. Incompatible avec
`POWERLINE_DIRECTION=rtl`.

![Invite sur une ligne](screenshot-oneline.png)


`POWERLINE_MAILDIR` définit le répertoire Maildir à surveiller (le répertoire
effectivement surveillé sera `$POWERLINE_MAILDIR/new`).


## `▶ Configuration avancée`

Ces options doivent être configurées avant de sourcer powerline.bash:

`POWERLINE_DIRECTION`. Si `rtl`, affiche les segments à droite, comme une zone
de notification.

![Invite de droite à gauche](docs/screenshot-rtl.png)

`POWERLINE_PALETTE` détermine la palette de couleur utilisée. Il y a deux
palettes prédéfinies : `8bit` et `256colors`. `24bit` est un alias de
`256color`. La palette par défaut est calculée en fonction des variables
`COLORTERM`, `TERM` et `OLDTERM`. Voici un exemple de la palette `8bit`.

![Palette 8bit](docs/screenshot-8bit.png)


### `▶ Personnaliser ses icônes`

Pour changer ou corriger une icône, utiliser la variable shell
`POWERLINE_ICONS_OVERRIDES`.

``` bash
# .bashrc
declare -A POWERLINE_ICONS_OVERRIDES
POWERLINE_ICONS_OVERRIDES=(
    [sep]=$'\uE0BC'
    [sep-fin]=$'\uE0BD'
)
. .config/powerline.bash
```

Ce qui donne:

![Séparateur personnalisés](screenshot-sep.png)

Pour connaître l'identifiant d'une icône dans powerline.bash, exécuter le
script `bin/icônes.sh`.

``` console
$ POWERLINE_ICONS=compat bin/icônes.sh
declare -x POWERLINE_ICONS='compat'

docker
etckeeper
fail             ✘
git
git-detached
home             ~
hostname
k8s              *
newmail          M
openstack
pwd
python
sep              ▶
sep-fin          ❯
$
```

Pour tester votre configuration d'icônes, sourcez le script `bin/icônes.sh`.

Pour trouver le code d'un icône, accédez au [navigateur icons-in-terminal] ou
l'[anti-sèche de Nerd Fonts].

[navigateur icons-in-terminal]: https://bersace.gitlab.io/powerline.bash/icons-in-terminal.html
[anti-sèche de Nerd Fonts]: https://www.nerdfonts.com/cheat-sheet?set=nf-linux-


### `▶ Personnaliser les couleurs`

Le script `bin/palette.sh` affiche la palette par défaut. Sourcer le script
affiche la palette courante. Le script permet également de connaître les
identifiants de couleurs : `rouge`, `violet`, `pwd-fond`, `pwd-texte`, etc.

Le principe est de définir des couleurs nommée (blanc, noir, jaune, etc.) et
d'utiliser ensuite des alias sémantiques pour les segments (pwd-fond,
pwd-texte, etc.). powerline.bash résouds les alias de couleurs à
l'initialisation pour utiliser directement des codes couleurs à l'exécution.

Pour surcharger les couleurs, définir le tableau associatif `POWERLINE_COLORS`
avant de sourcer `powerline.bash`. Les code couleurs sont définies en couleur
de fond `48;...` et non en couleur de texte `38;...`. powerline.bash se charge
de basculer la couleur sur le fond ou le texte.

``` bash
declare -A POWERLINE_COLORS
POWERLINE_COLORS=(
    [rouge]="48;5;148"  # Remplace le rouge par du vert...
    [pwd-fond]=rouge
)
. powerline.bash
```


### `▶ Personnaliser la couleur du segment hostname`

Bien que la couleur du segment `hostname` soit calculée en fonction de son
contenu, il est possible de passer outre ce fonctionnement en spécifiant ses
couleurs de fond et de texte favorites avant d'invoquer `powerline.bash`:

```bash
POWERLINE_HOSTNAME_BG="48;5;172"
POWERLINE_HOSTNAME_FG="38;5;015"
. powerline.bash
```

Il est également possible d'utiliser les codes couleur de la palette :

```bash
POWERLINE_HOSTNAME_BG="blanc"
POWERLINE_HOSTNAME_FG="bleu-canard"
. powerline.bash
```


## `▶ Créer un segment personnalisé`

Un segment est constitué de deux fonctions : `__powerline_init_*` et
`__powerline_segment_*`. Seule la fonction `__powerline_segment_*` est
obligatoire.

La fonction `__powerline_segment_*` envoie les segments à afficher dans un
tableau bash `__powerline_retval`. Chaque segment est une chaîne d'informations
séparées par `:` dans l'ordre suivant :
`<couleur-icône>:<icône>:<couleur-fond>:<couleur-texte>:<texte>`.

- Une couleur est soit une code couleur de texte RGB : `48;5;<R>;<G>;<B>`, soit
  un identifiant de couleur disponible dans le tableau associatif
  `POWERLINE_COLORS` comme `jaune`. Le script `bin.palette.sh` liste les
  identifiants de couleurs et leur valeurs.
- L'icône est vide ou un code d'Emoji.
- Le texte est vide ou un texte libre.

Voici un exemple de segment:

``` bash
__powerline_segment_test() {
    __powerline_retval=(
        "jaune:X:bleu:blanc:Mon texte"
    )
}
```

Pour l'activer, ajouter `test` à la variable `POWERLINE_SEGMENTS`. Ce segment
affichera un fond bleu avec un X jaune et "Mon texte" en blanc.

Si la fonction `__powerline_segment_init` existe, elle est appelée lorsque
`powerline.bash` est sourcé.

N'hésitez pas à partager votre segment via un patch pour avoir une relecture du
code et peut-être le partager à d'autres ! La langue de travail du projet est
le Français : les commentaires, les messages de commits doivent être en
Français.


## `▶ Problèmes de polices`

Le projet Powerlevel9k a une [page wiki fournie sur les problèmes
d'affichage](https://github.com/Powerlevel9k/powerlevel9k/wiki/Troubleshooting)
des caractères Powerline.

Le site [s9w](http://s9w.io/font_compare/) compare visuellement les
polices pour vous aider à choisir celle qui vous convient.

Installer la police choisie avec votre gestionnaire de paquet favori, le
gestionnaire de police de votre système ou manuellement en copiant le fichier
de la police dans le dossier ad-hoc.


### `▶ Configurer GNOME`

Un exemple d'installation manuelle avec Gnome:
- Téléchargement des .ttf originaux (icons-in-terminal et JetBrains Mono)
- Patch minimal avec nerd-font : juste powerline extra symbols.
- Installation des polices avec gnome-font-viewer.
- Configuration dans gnome-tweak-tools
- Configuration de fontconfig. (exemple: [10-bersace.conf](https://gitlab.com/bersace/config/-/blob/master/files/.config/fontconfig/conf.d/10-bersace.conf))


### `▶ Configurer XTerm`

La configuration d'une police vectorielle pour xterm peut être un peu complexe,
donc voici les grandes lignes.

1. Trouver le *nom exact* de la police
   avec `fc-list` de fontconfig. Exemple:
   ``` console
   $ fc-list : family style file | grep FiraMono
   ...
   /usr/share/fonts/nerd-fonts-complete/TTF/Sauce Code Pro Semibold Nerd Font Complete.ttf: FiraMono Nerd Font:style=Regular
   ...
   ```
2. Tester la police avec xterm: `xterm -fa "FiraMono Nerd
   Font:style=Semibold:size=12"`
3. Configurer cette police dans .Xresources:
   ```
   XTerm*faceName: FiraMono Nerd Font Mono
   XTerm*faceSize: 12
   XTerm*renderFont: true
   ```
4. Charger ces ressources avec `xrdb -merge ~/.Xresources` et relancer xterm.
