#!/usr/bin/env bash
set -eu

LOGNAME=utilisateur
HOSTNAME=machine
. ./powerline.bash

for RVB in {0..4}{1..3}{1..4} ; do
    DEBUG=o __powerline_get_foreground ${RVB::1} ${RVB:1:1} ${RVB:2:1}
    echo
done
