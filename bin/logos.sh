#!/bin/bash -eu
#
# Génère le segment logo pour tous les systèmes connus.
#

generer() {
	# Génère une invite de commande et l'affiche.

	POWERLINE_SEGMENTS="$1"
	__powerline_init_segments
	__update_ps1

	# Nettoyer les \[\]
	PS1="${PS1//\\]/}"
	PS1="${PS1//\\[/}"
	# Remplacer \$ par $
	PS1="${PS1/\\\$/$}"

	# Afficher l'invite, indentée de 4 espaces.
	echo -e "    ${PS1/\\n/\\n    }"
}

POWERLINE_ICONS=${POWERLINE_ICONS-icons-in-terminal}
# shellcheck source=/dev/null
. ./powerline.bash

# C'est parti pour la séance photo!

echo

logos=(
	alpine
	apple
	arch
	centos
	debian
	elementary
	fedora
	freebsd
	gentoo
	linuxmint
	manjaro
	opensuse
	ubuntu
	redhat
	slackware
	windows
	linux
	pouet
)

for POWERLINE_LOGO_ID in "${logos[@]}" ; do
	generer "logo"
done

echo
echo
echo
