#!/usr/bin/env bash
set -eu

. powerline.bash

for h in {0..59} ; do
    __powerline_hostname_color256 $h
	rgb=("${__powerline_retval[@]}")
    __powerline_color256 "${rgb[@]}"
	fond="${__powerline_retval[0]}"
    __powerline_get_foreground "${rgb[@]}"
    texte="${__powerline_retval[0]}"

    printf -v etiquette "h%d@rgb%d%d%d" $h "${rgb[@]}"

    echo -en "\\e[48;5;${fond};38;5;${texte}m $etiquette \\e[0;38;5;${fond};48;5;31m"$'\uE0B0'""
    echo -en "\\e[48;5;31;38;5;15m ~ \\e[0;38;5;31;48;5;236m"$'\uE0B0'"\\e[0m"
    echo -en "\\e[48;5;236;38;5;15m src / powerline.bash \\e[0;38;5;236m"$'\uE0B0'"\\e[0m"
    echo -e "\\e[0m"
    echo '$ '
done
