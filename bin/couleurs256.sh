#!/usr/bin/env bash
#
# Affiche la palette des 256 couleurs RGB
#
set -eu

afficher_couleur() {
	local couleur="$1"
        printf -v texte "%7s" "${2-$couleur}"
        echo -en "\\e[48;5;${couleur};38;5;0m ${texte}"
        echo -en "\\e[38;5;15m ${texte} "
}

for rvb in {0..5}{0..5}{0..5} ; do
	r=${rvb:0:1}
	v=${rvb:1:1}
	b=${rvb:2:1}

	couleur=$(( 16 + "$r" * 36 + "$v" * 6 + "$b"))
	afficher_couleur "$couleur" "$couleur=$r$v$b"

	if [ "$b" -eq "5" ] ; then
		    echo -e "\\e[0m"
	fi
done

echo

for couleur in {232..255} ; do
	afficher_couleur "$couleur"
        if [ "$(( (3 + couleur) % 6))" -eq 0 ] ; then
                echo -e "\\e[0m"
        fi
done
