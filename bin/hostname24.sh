#!/usr/bin/env bash
set -eu

# shellcheck source=/dev/null
. powerline.bash

for class in root virtualmachine local remote pouetpouet; do
	case "$class" in
		local)
			num=$((2 * 7))
			;;
		remote)
			num=$((3 * 7))
			;;
		container|virtualmachine)
			num=$((9 * 7))
			;;
		root)
			num=$((3 * 4))
			;;
		*)
			num=1
			;;
	esac


	for h in $(seq 0 $(( num - 1 ))) ; do
		__powerline_hostname_color24 "$class" "$h"
		hue="${__powerline_retval[0]}"
		sat="${__powerline_retval[1]}"

		__powerline_hsl2rgb "${__powerline_retval[@]}"
		r="${__powerline_retval[0]}"
		v="${__powerline_retval[1]}"
		b="${__powerline_retval[2]}"

		printf -v etiquette "h%03d@${hue/0.},${sat/0.}.%02x%02x%02x" "$h" "$r" "$v" "$b"
		fond="2;$r;$v;$b"
		texte="5;253"
		echo -en "\\e[48;${fond};38;${texte}m $etiquette \\e[0;38;${fond};48;5;31m"$'\uE0B0'""
		echo -en "\\e[48;5;31;38;5;15m ~ \\e[0;38;5;31;48;5;236m"$'\uE0B0'"\\e[0m"
		echo -en "\\e[48;5;236;38;5;15m src / powerline.bash / $class\\e[0;38;5;236m"$'\uE0B0'"\\e[0m"
		echo -e "\\e[0m"
		echo '$ '
	done
done

echo
